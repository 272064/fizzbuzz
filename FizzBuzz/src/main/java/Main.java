import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in =  new Scanner(System.in);
        System.out.print("Inserire un numero intero positivo: ");
        int numero = in.nextInt();

        FizzBuzz fizzbuzz = new FizzBuzz(numero);
        System.out.println(fizzbuzz.getSequence());
    }
}