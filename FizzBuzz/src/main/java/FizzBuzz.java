public class FizzBuzz {
    public static final String FIZZ = "fizz";
    public static final String BUZZ = "buzz";
    private Integer n;
    public FizzBuzz(Integer n){
        this.n = n;
    }

    public String getSequence(){
        StringBuilder risposta = new StringBuilder();
        for(int i=0; i<n; i++){
            risposta.append(getSequenceItem(i+1));
            risposta.append(" ");
        }

        return risposta.toString().trim();
    }

    private String getSequenceItem(Integer numero){
        boolean fizz = (numero%3) == 0;
        boolean buzz = (numero%5) == 0;
        StringBuilder risposta = new StringBuilder();

        if(fizz){
            risposta.append(FIZZ);
        }

        if(buzz){
            risposta.append(BUZZ);
        }

        if(!fizz && !buzz)
            risposta.append(numero);

        return risposta.toString();
    }
}