import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {
    @Test
    void feature_1() {
        FizzBuzz fizzbuzz = new FizzBuzz(1);
        assertEquals("1",fizzbuzz.getSequence());
    }

    @Test
    void feature_3() {
        FizzBuzz fizzbuzz = new FizzBuzz(3);
        assertEquals("1 2 fizz",fizzbuzz.getSequence());
    }

    @Test
    void feature_5() {
        FizzBuzz fizzbuzz = new FizzBuzz(5);
        assertEquals("1 2 fizz 4 buzz",fizzbuzz.getSequence());
    }

    @Test
    void feature_15() {
        FizzBuzz fizzbuzz = new FizzBuzz(15);
        String stringaAspettata = "1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz";
        assertEquals(stringaAspettata,fizzbuzz.getSequence());
    }

}