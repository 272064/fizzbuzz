# FizzBuzz

Classico esercizio in cui dato in unput un numero, vengono stampati tutti i numero tra 1 ed n. <br/>
Invece di multipli di 3 stampa Fizz. <br/>
Invece di multipli di 5 stampa Buzz. <br/>
In caso di multipli sia di 3 che di 5 stampa FizzBuzz

# Commento

Credo che questa esperienza sia stata molto utile.
Ho imparato ad usare meglio junit su intellij e mi sono allenato ulteriormente con git, dovendo anche risolvere un paio di errori riguardanti git checkout che non funzionava correttamente e nel merge una risoluzione delle collisioni non prevista.
Credo che l'esperienza sia stata utile, soprattutto per git. <br/>
Come voto darei un 4/5.
